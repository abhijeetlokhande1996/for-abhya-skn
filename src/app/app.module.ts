import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PersonalComponent } from './personal/personal.component';
import { ProfessionalComponent } from './professional/professional.component';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {
    path: 'personal',
    component: PersonalComponent
  }, {
    path: 'professional',
    component: ProfessionalComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PersonalComponent,
    ProfessionalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
